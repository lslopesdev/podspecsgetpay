Pod::Spec.new do |spec|

  spec.module_name  = "GetnetCards"
  spec.name         = "GetnetCards"
  spec.version      = "0.0.4"
  spec.summary      = "A Getnet framework."
  spec.homepage     = "https://www.superget.com.br/"

  spec.description  = "GetnetCards"

  #spec.license      = { :type => "MIT", :file => "LICENSE" }

  spec.author       = { "Ludgero Mascarenhas" => "ludgeromascarenhas_datum@getnet.com.br" }

  spec.platform     = :ios, "11.0"

  spec.source       = { :git => "https://bitbucket.getnet.com.br/scm/sgm/module-cards-ios.git", :tag => spec.version }

  spec.exclude_files = "Classes/Exclude"

  spec.license 		  = { :type => 'MIT', :file => "LICENSE" }

  spec.swift_version = "5"

  spec.dependency 'Getpay-Core'
 
  spec.source_files = ["#{spec.name}/**/*.{swift}", "#{spec.name}/**/*.{h}", "#{spec.name}/**/*.{m}"]

  spec.public_header_files = '*.h'

  spec.resources = ["#{spec.name}/**/*.{png}", "#{spec.name}/**/*.{jpeg}", "#{spec.name}/**/*. {jpg}",  "#{spec.name}/**/*.{pdf}", "#{spec.name}/**/*.{storyboard}", "#{spec.name}/**/*.{ttf}", "#{spec.name}/**/*.{xib}", "#{spec.name}/**/*.{json}", "#{spec.name}/**/*.{xcassets}", "#{spec.name}/**/*.{strings}"]

end
